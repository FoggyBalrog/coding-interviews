# Coding Interviews

<img src="./code.png" height="100px">

Small coding tests for technical interviews.

## License

The source code is licensed under the terms of the [GNU Affero General Public License Version 3](./LICENSE.txt).

## Credits

Project icon by [Prosymbols](https://www.flaticon.com/authors/prosymbols).
